package com.example.networkvibelab.retrofit

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.example.networkvibelab.data.Post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object PostsResponse {
    lateinit var posts: List<Post>
    var state by mutableStateOf("WAITING")
        private set

    init {
        getResponse()
    }

    private fun getResponse() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(ApiService::class.java)
        CoroutineScope(Dispatchers.IO).launch {
            posts = service.getPosts()
            state = "READY"
        }
    }

    fun updatePost(post: Post){
        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(ApiService::class.java)
        CoroutineScope(Dispatchers.IO).launch {
            service.updatePost(post.id, post)
        }
    }
}