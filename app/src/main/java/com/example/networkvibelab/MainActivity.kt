package com.example.networkvibelab

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.example.networkvibelab.data.Post
import com.example.networkvibelab.retrofit.PostsResponse
import com.example.networkvibelab.ui.theme.NetworkVIBELABTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NetworkVIBELABTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    HomeScreen()
                }
            }
        }
    }
}

@Composable
fun HomeScreen(modifier: Modifier = Modifier) {
    when(PostsResponse.state) {
        "READY" -> MessageList(modifier = modifier)
        "WAITING" -> LoadingScreen(modifier = modifier)
    }
}

@Composable
fun MessageList(
    modifier: Modifier = Modifier
) {
    LazyColumn(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = modifier
    ) {
        items(PostsResponse.posts) {post ->
            CardWithMessage(
                id = post.id,
                userId = post.userId,
                title = post.title,
                body = post.body,
                modifier = Modifier
                    .padding(
                        top = 6.dp,
                        bottom = 6.dp,
                        start = 36.dp,
                        end = 36.dp
                    )
            )
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CardWithMessage(
    id: Int, title: String, body: String, userId: Int,
    modifier: Modifier = Modifier
    ) {
    val (showDialog, setShowDialog) =
        remember { mutableStateOf(false) }
    Card(
        modifier = modifier,
        onClick = {setShowDialog(true)}
    ) {
        Row(
        ) {
            Column (
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.weight(0.3f)
            ){
                Text(
                    text = id.toString(),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
                Divider(
                    thickness = 1.dp,
                    color = Color.Black
                )
                Text(
                    text = title,
                    fontSize = 18.sp,
                    modifier = Modifier
                        .padding(6.dp),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.primary
                )
            }

            Text(
                text = body,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .weight(0.7f)
                    .padding(6.dp)
            )
        }
    }
    EditDialog(
        showDialog = showDialog,
        setShowDialog = setShowDialog,
        title = title,
        body = body,
        id = id,
        userId = userId
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditDialog(
    showDialog: Boolean = false,
    setShowDialog: (Boolean) -> Unit = {},
    title: String = "title",
    body: String = "body",
    id: Int = 1,
    userId: Int = 3,
    modifier: Modifier = Modifier
) {
    val context = LocalContext.current
    var editTitle by remember {
        mutableStateOf(title)
    }
    var editBody by remember {
        mutableStateOf(body)
    }

    if (showDialog) {
        Dialog(
            onDismissRequest = { setShowDialog(false) }
        ) {
            Card(
                modifier = modifier
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.padding(top = 20.dp)
                ) {
                    TextField(
                        value = editTitle,
                        onValueChange = {editTitle = it},
                        keyboardOptions = KeyboardOptions.Default.copy(
                            imeAction = ImeAction.Next
                        )
                    )
                    TextField(
                        value = editBody,
                        onValueChange = {editBody = it}
                    )
                    Spacer(modifier = Modifier.size(36.dp))
                    Row(
                        horizontalArrangement = Arrangement.SpaceAround,
                        modifier = Modifier
                            .padding(bottom = 20.dp)
                            .fillMaxWidth()
                    ){
                        Button(onClick = {
                            if (editBody != body || editTitle != title) {
                                val post = Post(body, id, title, userId)
                                PostsResponse.updatePost(post)
                                Toast.makeText(context,
                                    "Post-запрос выполнен",
                                    Toast.LENGTH_SHORT).show()
                            } else {
                                Toast.makeText(context,
                                    "Post-запрос не был выполнен (ничего не изменено)",
                                    Toast.LENGTH_SHORT).show()
                            }
                            setShowDialog(false)
                        }) {
                            Text(text = stringResource(R.string.apply))
                        }
                        Button(onClick = {
                            setShowDialog(false)
                        }) {
                            Text(text = stringResource(R.string.close))
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
    ) {
        Text(
            text = stringResource(R.string.download),
            fontSize = 36.sp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    NetworkVIBELABTheme {
        CardWithMessage(
            id = 1,
            userId = 3,
            title = "Hello, afdfsdfsdfsdfsfsdfsdfsdfsdfsdfsdf",
            body = "Hello world! FAFFDFSDFSDFSDFSDFSDFSDFSDFSDFSDFDSFSDFSDFSDFSDFSDFD" +
                    "sfsdfsdfsdfsdfsdfsdfsdfsdefsvhshdf7834fhdff83rfbhdfwfsfgsdjf3yfhd"
        )
    }
}
